# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.2.0-SNAPSHOT] 

- Updated maven-parent to 1.2.0

## [v1.1.1] - [2023-03-30]


- see  #24900 upgrade version just for removing the gcube-staging dependencies 
- removing old maps: cnr.servicemap edison.servicemap isti.servicemap preprodold.servicemap socialisti.servicemap

